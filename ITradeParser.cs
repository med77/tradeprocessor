﻿using System;
using System.Collections.Generic;

namespace dotnetcore
{
    public interface ITradeParser
    {
        IEnumerable<TradeRecord> Parse(IEnumerable<string> tradeData);
    }

    public class SimpleTradeParser : ITradeParser
    {
        private static float LotSize = 100000f;
        private bool Validate(string[] tradeData)
        {
            if (tradeData.Length != 3)
            {
                Console.WriteLine("Line malformed. Only {0} field(s) found.", tradeData.Length);
                return false;
            }

            if (tradeData[0].Length != 6)
            {
                Console.WriteLine("Trade currencies malformed: '{0}'", tradeData[0]);
                return false;
            }

            if (!int.TryParse(tradeData[1], out _))
            {
                Console.WriteLine("Trade amount not a valid integer: '{0}'", tradeData[1]);
                return false;
            }

            if (!decimal.TryParse(tradeData[2], out _))
            {
                Console.WriteLine("WARN: Trade price not a valid decimal:'{0}'", tradeData[2]);
                return false;
            }
            return true;
        }
        private TradeRecord MapTrade(string[] fields)
        {
            var sourceCurrencyCode = fields[0].Substring(0, 3);
            var destinationCurrencyCode = fields[0].Substring(3, 3);
            var tradeAmount = int.Parse(fields[1]);
            var tradePrice = decimal.Parse(fields[2]);
            var tradeRecord = new TradeRecord(sourceCurrencyCode, destinationCurrencyCode, tradeAmount / LotSize, tradePrice);
            return tradeRecord;
        }

        public IEnumerable<TradeRecord> Parse(IEnumerable<string>
        tradeData)
        {
            var trades = new List<TradeRecord>();
            foreach (var line in tradeData)
            {
                var fields = line.Split(new[] { ',' });
                if (!Validate(fields))
                    continue;
                var trade = MapTrade(fields);
                trades.Add(trade);
            }
            return trades;
        }
    }
}
