﻿namespace dotnetcore
{
    public class TradeProcessor
    {
        private readonly ITradeDataProvider tradeDataProvider;
        private readonly ITradeParser tradeParser;
        private readonly ITradeStorage tradeStorage;
        public TradeProcessor(ITradeDataProvider tradeDataProvider, ITradeParser tradeParser, ITradeStorage tradeStorage)
        {
            this.tradeDataProvider =
                tradeDataProvider;
            this.tradeParser = tradeParser;
            this.tradeStorage = tradeStorage;
        }
        public void ProcessTrades()
        {
            var lines = tradeDataProvider.GetTradeData();
            var trades = tradeParser.Parse(lines);
            tradeStorage.Persist(trades);
        }
    }

    public class TradeRecord
    {
        public TradeRecord(string sourceCurrency, string destinationCurrency, float lots, decimal price)
        {
            SourceCurrency = sourceCurrency;
            DestinationCurrency = destinationCurrency;
            Lots = lots;
            Price = price;
        }

        public string SourceCurrency { get; }
        public string DestinationCurrency { get; }
        public float Lots { get; }
        public decimal Price { get; }
    }
}

